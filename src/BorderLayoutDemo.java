import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

public class BorderLayoutDemo extends JFrame {

	public BorderLayoutDemo() {
		//Handles the layout and title of the JFrame
		setTitle("BorderLayout Demo");
		setSize(350, 125);
		setVisible(true);
		BorderLayout layout = new BorderLayout();
		this.setLayout(layout);
		setLocationRelativeTo(null);
		//Creates JPanels
		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		JPanel panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		//Creates JButtons and sets their titles
		JButton button1 = new JButton();
		button1.setText("Button 1");
		JButton button2 = new JButton();
		button2.setText("Button 2");
		JButton button3 = new JButton();
		button3.setText("Button 3");
		JButton button4 = new JButton();
		button4.setText("Button 4");
		JButton button5 = new JButton();
		button5.setText("Button 5");
		JButton button6 = new JButton();
		button6.setText("Button 6");
		//Adds buttons to the two different JPanels
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel2.add(button4);
		panel2.add(button5);
		panel2.add(button6);
		//Adds Panels to JFrame
		this.add(panel1, BorderLayout.PAGE_END);
		this.add(panel2, BorderLayout.CENTER);
		//Program ends upon the JFrame closing
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}
	public static void main(String[] args) {
		new BorderLayoutDemo();
	}
}
